#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void create();
void display();
void delete();
void bubbleSort();


struct Mahasiswa{
	int nim;
	char nama[30];
	char jurusan[50];
};

struct node{
	struct Mahasiswa mahasiswa;
	struct node *next;
};

struct node *head = NULL;
struct node *last = NULL;

int main()     
{
    int choice;
    char temp;
	struct Mahasiswa mahasiswa;
    while(1){
        system("cls");
               
        printf("                MENU                             \n");
        printf("1.Menambah data \n");
        printf("2.Tampilkan data    \n");;
        printf("3.Hapus semua data      \n");;
        printf("4.Exit       \n");
        printf("--------------------------------------\n\n");
        printf("Enter your choice:\t");
        scanf("%d",&choice);
        switch(choice)
        {
            case 1:
                printf("NIM: ");
                scanf("%d", &(mahasiswa.nim));
				printf("Nama: ");
				scanf("%c", &temp);
				scanf("%[^\n]", mahasiswa.nama);
				printf("Jurusan: ");
				scanf("%c", &temp);
				scanf("%[^\n]", mahasiswa.jurusan);
				create(mahasiswa);
                break;                  
        	case 2:
                bubbleSort();
                display();
                break;
            case 3: 
                delete();
                break;                
            case 4:
                exit(0);
                break;
            default:
                printf("n Wrong Choice:n");
				break;
        }
        printf("\n\nPress any key to continue..");
        getch();
    }
    return 0;
}



int i = NULL;

void create(struct Mahasiswa mahasiswa){
	if(i < 5){
		if(head == NULL){
			struct node * temp=(struct node*)malloc(sizeof(struct node));
			temp->mahasiswa = mahasiswa;
			temp->next = NULL;
			head = temp;
			last = temp;
		}
		else{
			struct node * temp=(struct node*)malloc(sizeof(struct node));
			temp->mahasiswa = mahasiswa;
			temp->next = NULL;
			last->next = temp;
			last = temp;
		}
		i++;
	}
	else{
		printf("\nTidak dapat menambah data");
	}		
}


void bubbleSort()
{
    struct node *a, *b;
    struct node *temp=(struct node*)malloc(sizeof(struct node));
	
	if(head != NULL){
		for(a = head; a->next != NULL; a = a->next){
	    	for(b = a->next; b != NULL; b = b->next){
	    		if(a->mahasiswa.nim > b->mahasiswa.nim){
	    			temp->mahasiswa = a->mahasiswa;
	    			a->mahasiswa = b->mahasiswa;
	    			b->mahasiswa = temp->mahasiswa;
				}
			}
		}
	}
}


void display(){
	if(head == NULL){
        printf("\nData tidak ditemukan\n");
    }
	else{
        struct node*current = head;
        printf("|NIM       |Nama                          |Jurusan                                           |\n");
        printf("|----------|------------------------------|--------------------------------------------------|\n");
        while(current!=NULL){
            printf("|%10d|%30s|%50s|\n", current->mahasiswa.nim, current->mahasiswa.nama, current->mahasiswa.jurusan);
            current = current->next;
        }
    }
}


void delete(){
	
	struct node *temp;

    while(head != NULL)
    {
        temp = head;
        head = head->next;

        free(temp);
    }
	
	//reset i menjadi null
	i = NULL;
    printf("\nData berhasil dihapus\n");
}

